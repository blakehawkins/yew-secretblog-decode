#!/usr/bin/env bash
set -euxo pipefail

wasm-pack build --target web
rollup ./main.js --format iife --file bundle.js
