import init, { main } from './pkg/yew_secretblog_decode.js';
async function ep() {
   await init('/pkg/yew_secretblog_decode_bg.wasm');
   document.querySelectorAll('.yew-decodeable').forEach(e => {
      main(e);
   });
}
ep()
