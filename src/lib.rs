use wasm_bindgen::prelude::*;
use web_sys::{Node, Element};
use yew::prelude::*;
use yew::virtual_dom::VNode;

struct Model {
    link: ComponentLink<Self>,
    value: String,
    init: String,
}

enum Msg {
    GotInput(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Properties)]
struct RawHtmlProps {
    pub inner_html: String,
}

struct RawHtml {
    props: RawHtmlProps,
}

impl Component for RawHtml {
    type Message = Msg;
    type Properties = RawHtmlProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let div = web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .create_element("div")
            .unwrap();
        div.set_inner_html(&self.props.inner_html[..]);

        let node = Node::from(div);
        VNode::VRef(node)
    }
}

#[derive(Clone, Properties)]
struct DecodeProps {
    val: String,
}

impl Component for Model {
    type Message = Msg;
    type Properties = DecodeProps;
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            value: "".into(),
            init: props.val,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::GotInput(v) => self.value = v,
        }
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div>
                <input type="text" value=&self.value oninput=self.link.callback(|e: InputData| Msg::GotInput(e.value)) />
                <RawHtml inner_html={ decode(self.value.clone(), self.init.clone()).unwrap_or_else(|e| e) } />
            </div>
        }
    }
}

fn decode(passphrase: String, input: String) -> Result<String, String> {
    let bytes = orion::aead::open(
        &orion::aead::SecretKey::from_slice(format!("{:<32}", passphrase).as_bytes())
            .map_err(|_| "failed to build secret key")?,
        &base64::decode(input).map_err(|_| "Failed to decode input")?,
    )
    .map_err(|_| "Failed to decrypt blob")?;

    String::from_utf8(bytes).map_err(|_| "failed to produce utf-8 from decrypted message".into())
}

#[wasm_bindgen]
pub fn main(container: Element) {
    yew::initialize();
    App::<Model>::new().mount_with_props(
        container.clone(),
        DecodeProps {
            val: container.text_content().unwrap(),
        },
    );
    yew::run_loop();
}
