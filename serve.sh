#!/usr/bin/env bash
set -euxo pipefail
./build.sh

http || cargo install https && http
